
#include <vr_system.hpp>

#include <bg/base/exception.hpp>
#include <bg/base/exception.hpp>
#include <bg/wnd/main_loop.hpp>
#include <bg/engine/openglCore/texture_impl.hpp>
#include <bg/log.hpp>
#include <bg/wnd/message_box.hpp>

#include <bg/scene/scene.hpp>

#include <thread>

VRSystem * VRSystem::s_singleton = nullptr;

VRSystem::VRSystem()
{
	s_singleton = this;
	vr::EVRInitError err = vr::VRInitError_None;
	_vrSystem = vr::VR_Init(&err, vr::VRApplication_Scene);

	if(err!= vr::VRInitError_None) {
		_vrSystem = nullptr;
		throw bg::base::InitException("Unable to initialize VR rintime: " + std::string(vr::VR_GetVRInitErrorAsEnglishDescription(err)));
	}
}


void VRSystem::init(bg::scene::Node * sceneRoot) {
	vr::EVRInitError eError = vr::VRInitError_None;
	_vrRenderModels = static_cast<vr::IVRRenderModels *>(vr::VR_GetGenericInterface(vr::IVRRenderModels_Version, &eError));
	if(!_vrRenderModels) {
		_vrSystem = nullptr;
		vr::VR_Shutdown();
		throw bg::base::InitException("Unable to get render model interface: " + std::string(vr::VR_GetVRInitErrorAsEnglishDescription(eError)));
	}

	_driver = getTrackedDeviceString(_vrSystem, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_TrackingSystemName_String);
	_display = getTrackedDeviceString(_vrSystem, vr::k_unTrackedDeviceIndex_Hmd, vr::Prop_SerialNumber_String);

	std::memset(_deviceClass, 0, sizeof(char) * vr::k_unMaxTrackedDeviceCount);

	_near = 0.1f;
	_far = 30.0f;



	bg::wnd::MainLoop::Get()->addCustomLoopClosure([&]() {
		vr::VREvent_t event;
		while(_vrSystem->PollNextEvent(&event, sizeof(event))) {
			processVREvent(event);
		}
	});

	vr::EVRInitError peError = vr::VRInitError_None;
	int elapsed = 0;
	while(!vr::VRCompositor()) {
		using namespace std::chrono_literals;
		bg::wnd::MessageBox::Response res = bg::wnd::MessageBox::Show(nullptr,"Open SteamVR",
			"Please, open SteamVR and click OK to continue, or press Cancel to quit.",
			bg::wnd::MessageBox::kButtonOkCancel);
		if (res==bg::wnd::MessageBox::kResponseCancel)
		{
			throw bg::base::InitException("Could not initialize VR compositor.");
		}
	}

	// Init tracking devices
	_controllersFound = 0;
	initControllers(sceneRoot);
	

	// Init cameras
	initCameras(sceneRoot);
}

void VRSystem::frame() {
	Controller::UpdateAll();

	_leftCamera->setProjection(getHMDProjection(vr::Eye_Left));
	_leftCamera->transform()->setMatrix(_matDevicePose[vr::k_unTrackedDeviceIndex_Hmd]);
	_leftCamera->transform()->matrix().mult(getHMDPose(vr::Eye_Left));

	_rightCamera->setProjection(getHMDProjection(vr::Eye_Right));
	_rightCamera->transform()->setMatrix(_matDevicePose[vr::k_unTrackedDeviceIndex_Hmd]);
	_rightCamera->transform()->matrix().mult(getHMDPose(vr::Eye_Right));

	updateMatrixPose();
}

void VRSystem::navigateTo(const bg::math::Vector3 & point) {
	using namespace bg::scene;
	Node * stage = bg::base::ObjectRegistry::Get().findObjectOfType<Node>("stage");
	if (stage) {
		bg::math::Vector3 currentPos = stage->transform()->matrix().position();
		bg::math::Vector3 translation = currentPos - point;
		stage->transform()->matrix()
			.identity()
			.translate(translation);
	}
}

void VRSystem::draw(bg::render::Renderer * renderer, bg::scene::Node * sceneRoot) {
	renderer->pipeline()->setRenderSurface(_leftSurface.getPtr());
	renderer->draw(sceneRoot, _leftCamera.getPtr());

	bg::base::Texture * tex = _leftSurface->texture();
	GLuint texName = tex->impl<bg::engine::openglCore::TextureImpl>()->textureName();
	vr::Texture_t leftEyeTex = { (void*)texName, vr::API_OpenGL, vr::ColorSpace_Gamma };

	vr::VRCompositor()->Submit(vr::Eye_Left, &leftEyeTex);

	renderer->pipeline()->setRenderSurface(_rightSurface.getPtr());
	renderer->draw(sceneRoot, _rightCamera.getPtr());

	tex = _rightSurface->texture();
	texName = tex->impl<bg::engine::openglCore::TextureImpl>()->textureName();
	vr::Texture_t rightEyeTex = { (void*)texName, vr::API_OpenGL, vr::ColorSpace_Gamma };
	
	vr::VRCompositor()->Submit(vr::Eye_Right, &rightEyeTex);
}

void VRSystem::destroy() {
	vr::VR_Shutdown();
}

void VRSystem::initControllers(bg::scene::Node * sceneRoot) {
	using namespace bg::scene;
	bg::base::Context * context = sceneRoot->context();
	for(vr::TrackedDeviceIndex_t index = vr::k_unTrackedDeviceIndex_Hmd + 1; index<vr::k_unMaxTrackedDeviceCount || _controllersFound==2; index += 1) {
		if(_vrSystem->GetTrackedDeviceClass(index) == vr::TrackedDeviceClass_Controller) {
			++_controllersFound;
			std::string sRenderModelName = getTrackedDeviceString(_vrSystem, index, vr::Prop_RenderModelName_String);
			Node * modelNode = new Node(context);
			modelNode->addComponent(new Transform());
			Controller::AddNew(this,index,modelNode);

			sceneRoot->addChild(modelNode);

			Drawable * modelDrawable = _modelDrawables[sRenderModelName].getPtr();
			if(!modelDrawable) {
				modelDrawable = VRModelFactory(context).loadModel(sRenderModelName);
				_modelDrawables[sRenderModelName] = modelDrawable;
				modelDrawable->setName("Controller A");
				modelNode->addComponent(modelDrawable);
			}
			else {
				modelNode->addComponent(modelDrawable->instance("Controller B"));
			}
		}
	}
}

void VRSystem::initCameras(bg::scene::Node * sceneRoot) {
	using namespace bg::scene;
	using namespace bg::math;
	bg::base::Context * context = sceneRoot->context();
	_vrSystem->GetRecommendedRenderTargetSize(&_renderWidth, &_renderHeight);

	Node * headNode = new Node(context);
	_headNode = headNode;
	sceneRoot->addChild(headNode);

	Node * leftCameraNode = new Node(context);
	leftCameraNode->setId("left-camera");
	leftCameraNode->addComponent(new Camera());
	leftCameraNode->addComponent(new Transform());
	_leftCamera = leftCameraNode->camera();
	headNode->addChild(leftCameraNode);
	_leftCamera->setProjection(getHMDProjection(vr::Eye_Left));
	_leftCamera->setViewport(Viewport(0, 0, _renderWidth, _renderHeight));

	_leftSurface = new bg::base::TextureRenderSurface(context);
	_leftSurface->create();
	_leftSurface->setSize(bg::math::Size2Di(_renderWidth, _renderHeight));

	Node * rightCameraNode = new Node(context);
	rightCameraNode->setId("right-camera");
	rightCameraNode->addComponent(new Camera());
	rightCameraNode->addComponent(new Transform());
	_rightCamera = rightCameraNode->camera();
	headNode->addChild(rightCameraNode);
	_rightCamera->setProjection(getHMDProjection(vr::Eye_Right));
	_rightCamera->setViewport(Viewport(0, 0, _renderWidth, _renderHeight));

	_rightSurface = new bg::base::TextureRenderSurface(context);
	_rightSurface->create();
	_rightSurface->setSize(bg::math::Size2Di(_renderWidth, _renderHeight));
}

void VRSystem::updateMatrixPose() {
	vr::VRCompositor()->WaitGetPoses(_trackedDevicePose, vr::k_unMaxTrackedDeviceCount, nullptr, 0);
	_validPoseCount = 0;
	_poseClasses = "";
	for(auto device = 0; device<vr::k_unMaxTrackedDeviceCount; ++device) {
		if(_trackedDevicePose[device].bPoseIsValid) {
			++_validPoseCount;
			_matDevicePose[device] = fromSteamVRMatrix(_trackedDevicePose[device].mDeviceToAbsoluteTracking);
			if(_deviceClass[device] == 0) {
				switch(_vrSystem->GetTrackedDeviceClass(device)) {
				case vr::TrackedDeviceClass_Controller:			_deviceClass[device] = 'C'; break;
				case vr::TrackedDeviceClass_HMD:				_deviceClass[device] = 'H'; break;
				case vr::TrackedDeviceClass_Invalid:			_deviceClass[device] = 'I'; break;
				case vr::TrackedDeviceClass_Other:				_deviceClass[device] = 'O'; break;
				case vr::TrackedDeviceClass_TrackingReference:	_deviceClass[device] = 'T'; break;
				default:										_deviceClass[device] = '?';
				}
			}
			_poseClasses += _deviceClass[device];
			if(Controller::Get(device)) {
				Controller::Get(device)->updatePose(_matDevicePose[device]);
			}
		}
	}

	if(_trackedDevicePose[vr::k_unTrackedDeviceIndex_Hmd].bPoseIsValid) {
		_hdmPose = _matDevicePose[vr::k_unTrackedDeviceIndex_Hmd];
	}
}

void VRSystem::processVREvent(const vr::VREvent_t & event) {
	using namespace bg::scene;
	using namespace bg::math;
	Controller * controller = nullptr;
	Controller::EventType type = Controller::kEventNone;
	Controller::ButtonIdentifier button = Controller::kButtonIdNone;
	if(event.eventType == vr::VREvent_ButtonPress) {
		controller = Controller::Get(event.trackedDeviceIndex);
		if (controller) {
			switch (event.data.controller.button) {
			case vr::k_EButton_ApplicationMenu:
				controller->_menuButton = Controller::kButtonPressed;
				button = Controller::kButtonIdMenu;
				break;
			case vr::k_EButton_Axis0:
				controller->_trackpadButton = Controller::kButtonPressed;
				button = Controller::kButtonIdTouchpad;
				break;
			case vr::k_EButton_Axis1:
				controller->_triggerButton = Controller::kButtonPressed;
				button = Controller::kButtonIdTrigger;
				break;
			case vr::k_EButton_Dashboard_Back:
				controller->_sideButton = Controller::kButtonPressed;
				button = Controller::kButtonIdSide;
				break;
			}
			type = Controller::kEventButtonPress;
		}
	}
	else if(event.eventType == vr::VREvent_ButtonUnpress) {
		controller = Controller::Get(event.trackedDeviceIndex);
		if(controller) {
			switch(event.data.controller.button) {
			case vr::k_EButton_ApplicationMenu:
				controller->_menuButton = Controller::kButtonReleased;
				button = Controller::kButtonIdMenu;
				break;
			case vr::k_EButton_Axis0:
				controller->_trackpadButton = Controller::kButtonReleased;
				button = Controller::kButtonIdTouchpad;
				break;
			case vr::k_EButton_Axis1:
				controller->_triggerButton = Controller::kButtonReleased;
				button = Controller::kButtonIdTrigger;
				break;
			case vr::k_EButton_Dashboard_Back:
				controller->_sideButton = Controller::kButtonReleased;
				button = Controller::kButtonIdSide;
				break;
			}
			type = Controller::kEventButtonRelease;
		}
	}
	else if(event.eventType == vr::VREvent_ButtonTouch) {
		controller = Controller::Get(event.trackedDeviceIndex);
		if(controller) {
			switch(event.data.controller.button) {
			case vr::k_EButton_ApplicationMenu:
				controller->_menuButton = Controller::kButtonTouched;
				button = Controller::kButtonIdMenu;
				break;
			case vr::k_EButton_Axis0:
				controller->_trackpadButton = Controller::kButtonTouched;
				button = Controller::kButtonIdTouchpad;
				break;
			case vr::k_EButton_Axis1:
				controller->_triggerButton = Controller::kButtonTouched;
				button = Controller::kButtonIdTrigger;
				break;
			case vr::k_EButton_Dashboard_Back:
				controller->_sideButton = Controller::kButtonTouched;
				button = Controller::kButtonIdSide;
				break;
			}
			type = Controller::kEventButtonTouch;
		}
	}
	else if(event.eventType == vr::VREvent_ButtonUntouch) {
		controller = Controller::Get(event.trackedDeviceIndex);
		if(controller) {
			switch(event.data.controller.button) {
			case vr::k_EButton_ApplicationMenu:
				controller->_menuButton = Controller::kButtonReleased;
				button = Controller::kButtonIdMenu;
				break;
			case vr::k_EButton_Axis0:
				controller->_trackpadButton = Controller::kButtonReleased;
				button = Controller::kButtonIdTouchpad;
				break;
			case vr::k_EButton_Axis1:
				controller->_triggerButton = Controller::kButtonReleased;
				button = Controller::kButtonIdTrigger;
				break;
			case vr::k_EButton_Dashboard_Back:
				controller->_sideButton = Controller::kButtonReleased;
				button = Controller::kButtonIdSide;
				break;
			}
			type = Controller::kEventButtonUntouch;
		}
	}
	if (controller && _controllerClosure) {
		_controllerClosure(controller,type,button);
	}
}

std::string VRSystem::getTrackedDeviceString(vr::IVRSystem * vr, vr::TrackedDeviceIndex_t dev, vr::TrackedDeviceProperty prop, vr::TrackedPropertyError * peError) {
	uint32_t unRequiredBufferLength = vr->GetStringTrackedDeviceProperty(dev, prop, nullptr, 0, peError);
	if(unRequiredBufferLength == 0) return "";
	char * pchBuffer = new char[unRequiredBufferLength];
	unRequiredBufferLength = vr->GetStringTrackedDeviceProperty(dev, prop, pchBuffer, unRequiredBufferLength, peError);
	std::string result = pchBuffer;
	delete[] pchBuffer;
	return result;
}


bg::math::Matrix4 VRSystem::getHMDProjection(vr::Hmd_Eye eye) {
	vr::HmdMatrix44_t mat = _vrSystem->GetProjectionMatrix(eye, _near, _far, vr::API_OpenGL);
	return bg::math::Matrix4(
		mat.m[0][0], mat.m[1][0], mat.m[2][0], mat.m[3][0],
		mat.m[0][1], mat.m[1][1], mat.m[2][1], mat.m[3][1],
		mat.m[0][2], mat.m[1][2], mat.m[2][2], mat.m[3][2],
		mat.m[0][3], mat.m[1][3], mat.m[2][3], mat.m[3][3]
	);
}

bg::math::Matrix4 VRSystem::getHMDPose(vr::Hmd_Eye eye) {
	vr::HmdMatrix34_t matEye = _vrSystem->GetEyeToHeadTransform(eye);
	bg::math::Matrix4 result(
		matEye.m[0][0], matEye.m[1][0], matEye.m[2][0], 0.0,
		matEye.m[0][1], matEye.m[1][1], matEye.m[2][1], 0.0,
		matEye.m[0][2], matEye.m[1][2], matEye.m[2][2], 0.0,
		matEye.m[0][3], matEye.m[1][3], matEye.m[2][3], 1.0f
	);
	return result;
}
