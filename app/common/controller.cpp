
#include <controller.hpp>
#include <vr_system.hpp>

#include <bg/physics/ray.hpp>
#include <bg/physics/plane.hpp>
#include <bg/physics/intersection.hpp>

#include <bg/log.hpp>


NavigationController::NavigationController(int controllerIndex)
	:_controllerIndex(controllerIndex)
	,_showRay(false)
{
}

NavigationController::~NavigationController() {
}

bg::scene::Component * NavigationController::clone() {
	return new NavigationController(_controllerIndex);
}

void NavigationController::init() {
	_rayPlist = new bg::base::PolyList(context());

	_rayPlist->addVertex(bg::math::Vector3(0.0f, 0.0f, 0.0f));
	_rayPlist->addVertex(bg::math::Vector3(0.0f, 0.0f, -30.0f));

	_rayPlist->addNormal(bg::math::Vector3(0.0f, 1.0f, 0.0f));
	_rayPlist->addNormal(bg::math::Vector3(0.0f, 1.0f, 0.0f));

	_rayPlist->addTexCoord0(bg::math::Vector2(0.0f, 0.0f));
	_rayPlist->addTexCoord0(bg::math::Vector2(0.0f, 0.0f));

	_rayPlist->addIndex(0);
	_rayPlist->addIndex(1);

	_rayPlist->setDrawMode(bg::base::PolyList::kLines);

	_rayPlist->build();

	_material = new bg::base::Material();
	_material->setDiffuse(bg::math::Color::Red());
	_material->setLightEmission(1.0f);
}

void NavigationController::frame(float delta) {
	//Controller * controller = Controller::Left();
	//if(controller && drawable()) {
	//	drawable()->material(0)->diffuse().a(1.0f - controller->triggerPosition());
	//}
}

void NavigationController::draw(bg::base::Pipeline * pipeline) {
	if(pipeline->shouldDraw(_material.getPtr()) && _showRay) {
		bg::ptr<bg::base::Material> currentMaterial = pipeline->effect()->materialPtr();
		pipeline->effect()->setMaterial(_material.getPtr());
		pipeline->draw(_rayPlist.getPtr());
		pipeline->effect()->setMaterial(currentMaterial.getPtr());
	}
}

void NavigationController::customEvent(const bg::base::CustomEvent &evt) {
	const ControllerEventData * data = evt.data<ControllerEventData>();
	if(data && data->controller()->index()==_controllerIndex) {
		switch(data->eventType()) {
		case Controller::kEventButtonRelease:
			if (data->button()==Controller::kButtonIdTouchpad) {
				navigate();
			}
			break;
		case Controller::kEventButtonTouch:
			if (data->button()==Controller::kButtonIdTouchpad) {
				_showRay = true;
			}
			break;
		case Controller::kEventButtonUntouch:
			if (data->button()==Controller::kButtonIdTouchpad) {
				_showRay = false;
			}
			break;
		}
	}
}

void NavigationController::navigate() {
	using namespace bg::physics;
	using namespace bg::math;
	if (transform()) {
		Vector4 p0(0.0f,0.0f,0.0f,1.0f);
		Vector4 p1(0.0f,0.0f,-30.0f,1.0f);
		p0 = transform()->matrix().multVector(p0);
		p1 = transform()->matrix().multVector(p1);

		Ray ray = Ray::RayWithPoints(p0.xyz(), p1.xyz());
		Plane plane;
		plane.setOrigin(Vector3());
		plane.setNormal(Vector3(0.0f, 1.0f, 0.0f));
	
		bg::log(bg::log::kDebug) << "Ray: " << ray.start().toString() << " to " << ray.end().toString() << bg::endl;
		bg::log(bg::log::kDebug) << "Plane: " << plane.toString() << bg::endl;

		RayToPlaneIntersection intersect = Intersection::RayToPlane(ray,plane);
		if (intersect.intersects()) {
			bg::log(bg::log::kDebug) << "Intersect in point " << intersect.endPoint().toString() << bg::endl;
			VRSystem::Get()->navigateTo(intersect.endPoint());
		}
	}
}


std::map<uint32_t, bg::ptr<Controller>> Controller::s_controllers;
uint32_t Controller::s_leftControllerIndex = vr::k_unMaxTrackedDeviceCount;
uint32_t Controller::s_rightControllerIndex = vr::k_unMaxTrackedDeviceCount;

Controller * Controller::Get(uint32_t index) {
	return s_controllers[index].getPtr();
}

Controller * Controller::AddNew(VRSystem * sys, uint32_t index, bg::scene::Node * n) {
	bg::ptr<Controller> ctrl = new Controller(sys, index, n);
	n->addComponent(new NavigationController(index));
	s_controllers[index] = ctrl.getPtr();
	if(s_leftControllerIndex == vr::k_unMaxTrackedDeviceCount) {
		s_leftControllerIndex = index;
	}
	else if(s_rightControllerIndex == vr::k_unMaxTrackedDeviceCount) {
		s_rightControllerIndex = index;
	}
	return ctrl.release();
}

void Controller::UpdateAll() {
	for(auto & c : s_controllers) {
		if(c.second.valid()) {
			c.second->update();
		}
	}
}

Controller * Controller::Left() {
	return Get(s_leftControllerIndex);
}

Controller * Controller::Right() {
	return Get(s_rightControllerIndex);
}

uint32_t Controller::LeftControllerIndex() {
	return s_leftControllerIndex;
}

uint32_t Controller::RightControllerIndex() {
	return s_rightControllerIndex;
}

void Controller::Destroy() {
	s_controllers.clear();
}

Controller::Controller(VRSystem * vr, uint32_t index, bg::scene::Node * node)
	:_vrSystem(vr)
	,_index(index)
	,_sceneNode(node)
{
}

void Controller::update() {
	vr::VRControllerState_t controllerState;
	_vrSystem->impl()->GetControllerState(_index,&controllerState);
	_triggerPosition = controllerState.rAxis[1].x;
	_trackpadPosition.set(controllerState.rAxis[0].x, controllerState.rAxis[0].y);
}

void Controller::updatePose(const bg::math::Matrix4 & mat) {
	_sceneNode->transform()->setMatrix(mat);
	_sceneNode->setEnabled(true);
}

float Controller::trackpadAngle() {
	if(!_trackpadPosition.isZero()) {
		float angle = bg::math::trigonometry::atan2(-_trackpadPosition.x(), _trackpadPosition.y()) + bg::math::kPiOver2;
		if(angle<0.0) {
			angle = bg::math::k2Pi - bg::math::kPiOver2 + (bg::math::kPiOver2 + angle);
		}
		return angle;
	}
	return 0.0f;
}

